package models

import play.api.libs.json.Json

case class Note(username: String, title: String, description: String) extends Model

object Note {
  implicit val noteFormat = Json.format[Note]
}
