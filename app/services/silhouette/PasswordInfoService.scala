package services.silhouette

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import repositories.silhouette.PasswordInfoRepo

import scala.concurrent.{ExecutionContext, Future}

trait PasswordInfoService {
  def add(loginInfo: LoginInfo, authInfo: PasswordInfo)(implicit ec: ExecutionContext): Future[Either[String, PasswordInfo]]
  def find(loginInfo: LoginInfo)(implicit ec: ExecutionContext): Future[Option[PasswordInfo]]
}

class PasswordInfoServiceImpl @Inject()(passwordInfoRepo: PasswordInfoRepo) extends PasswordInfoService {
  override def add(loginInfo: LoginInfo, authInfo: PasswordInfo)(implicit ec: ExecutionContext): Future[Either[String, PasswordInfo]] =
    passwordInfoRepo
      .add(loginInfo, authInfo)
      .map(passwordInfo => Right(passwordInfo))
      .recover {
        case e: Exception => Left(e.getMessage)
      }

  override def find(loginInfo: LoginInfo)(implicit ec: ExecutionContext): Future[Option[PasswordInfo]] = passwordInfoRepo.find(loginInfo)
}
