package services.io

import java.nio.file.Path
import javax.inject.Inject

import com.sksamuel.scrimage.nio.{GifWriter, ImageWriter, JpegWriter, PngWriter}
import repositories.io.FileIORepo

trait FileIOService {
  def scale(in: Path, out: Path, writer: ImageWriter): Path
  def getWriter(writer: Option[String]): Option[ImageWriter]
}

class FileIOServiceImpl @Inject()(fileIORepo: FileIORepo)
  extends FileIOService {

  def scale(in: Path, out: Path, writer: ImageWriter): Path = fileIORepo.scale(in, out, writer)

  def getWriter(writer: Option[String]): Option[ImageWriter] = writer.flatMap {
    case "image/jpeg" => Some(JpegWriter())
    case "image/gif" => Some(GifWriter())
    case "image/png" => Some(PngWriter())
    case _ => None
  }
}
