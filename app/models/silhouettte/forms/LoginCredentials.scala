package models.silhouettte.forms

import play.api.libs.json.{Json, OFormat}

case class LoginCredentials(username: String, password: String)

object LoginCredentials {
  implicit val loginCredentialsFormat: OFormat[LoginCredentials] = Json.format[LoginCredentials]
}
