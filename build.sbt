name := """todo-app"""
organization := "com.tda"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.2"
val silhouetteVersion = "5.0.0"
val playReactiveMongo = "0.12.6-play26"
val scalaGuice = "4.1.0"
val playS3Version = "9.0.0"
val scrimageVersion = "3.0.0-alpha4"

libraryDependencies ++= Seq(
  ws,
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test,
  "org.reactivemongo" %% "play2-reactivemongo" % playReactiveMongo,
  "com.mohiva" %% "play-silhouette" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-persistence" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-password-bcrypt" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-crypto-jca" % silhouetteVersion,
  "net.kaliber" %% "play-s3" % playS3Version,
  "com.sksamuel.scrimage" %% "scrimage-core" % scrimageVersion
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.tda.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.tda.binders._"
