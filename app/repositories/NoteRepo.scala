package repositories

import javax.inject.{Inject, Singleton}

import models.Note
import play.api.libs.json.{JsObject, Json}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.{Cursor, MongoConnection, ReadPreference}
import reactivemongo.play.json._

import scala.concurrent.{ExecutionContext, Future}

trait NoteRepo extends GenericRepo[Note] with MongoDb {
  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]]
}

@Singleton
class NoteRepoImpl @Inject()(val mongoConnection: MongoConnection) extends NoteRepo {

  private def noteColl(implicit ec: ExecutionContext) = connect(coll = "note")

  override def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]] = {
    noteColl.flatMap(_.insert(t))
      .map(_ => Right(t))
      .recover {
        case WriteResult.Message(msg) => Left(msg)
      }
  }

  override def find(implicit ec: ExecutionContext): Future[List[Note]] =
    genericFind(Json.obj(), Json.obj())
  override def findByUsername(username: String)(implicit ec: ExecutionContext): Future[List[Note]] =
    genericFind(Json.obj("username" -> username), Json.obj())

  override def update[T]: Unit = ???

  override def delete[T]: Unit = ???

  private def genericFind(find: JsObject, projection: JsObject)(implicit ec: ExecutionContext) = {
    noteColl
      .map(_.find(find, projection)
        .cursor[Note](ReadPreference.Primary))
      .flatMap(_.collect[List](-1, Cursor.FailOnError[List[Note]]()))
  }
}