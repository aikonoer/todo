package services.silhouette

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import models.silhouettte.UserDetails
import repositories.silhouette.UserInfoRepo
import services.GenericService

import scala.concurrent.{ExecutionContext, Future}

trait UserInfoService extends IdentityService[UserDetails] with GenericService[UserDetails]

class UserInfoServiceImpl @Inject()(userInfoRepo: UserInfoRepo) extends UserInfoService {

  override def retrieve(loginInfo: LoginInfo): Future[Option[UserDetails]] = userInfoRepo.findByUsername(loginInfo)
  override def insert(t: UserDetails)(implicit ec: ExecutionContext): Future[Either[String, UserDetails]] = userInfoRepo.insert(t)
}
