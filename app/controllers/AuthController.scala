package controllers

import javax.inject._

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasher}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import models.silhouettte.UserDetails
import models.silhouettte.forms.LoginCredentials
import play.api.i18n.{Messages, MessagesApi}
import play.api.libs.json._
import play.api.mvc._
import reactivemongo.play.json._
import services.silhouette.{PasswordInfoService, UserInfoService}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthController @Inject()(messagesApi: MessagesApi,
                               cc: ControllerComponents,
                               userInfoService: UserInfoService,
                               passwordInfoService: PasswordInfoService,
                               passwordHasher: PasswordHasher,
                               credentialsProvider: CredentialsProvider)
  extends AbstractController(cc) with MyBaseController{

  implicit lazy val ec: ExecutionContext = defaultExecutionContext

  def signUp(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[LoginCredentials].fold(
      error => Future.successful(BadRequest(Json.obj("Status" -> "Error", "message" -> JsError.toJson(error)))),
      loginCredentials => {
        val loginInfo = LoginInfo(CredentialsProvider.ID, loginCredentials.username)
        for {
          ou <- userInfoService.retrieve(loginInfo)
          if ou.isEmpty
          e <- userInfoService.insert(UserDetails(loginCredentials.username))
          if e.isRight
          either <- passwordInfoService.add(loginInfo, passwordHasher.hash(loginCredentials.password))
        } yield either match {
          case Right(_)     => Ok(okResponse(Messages("add.success", loginCredentials.username)))
          case Left(error)  => BadRequest(failResponse(Messages("add.failure", error)))
        }
      })
  }

  def authenticate(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[LoginCredentials].fold(
      error => Future.successful(BadRequest(Json.obj("Status" -> "Error", "message" -> JsError.toJson(error)))),
      loginCredentials =>
        credentialsProvider
        .authenticate(Credentials(loginCredentials.username, loginCredentials.password))
        .map(loginInfo => Ok(okResponse(Messages("authentication.success", loginInfo.providerKey))))
        .recover {
          case e: Exception => BadRequest(failResponse(Messages("authentication.failure", e.toString)))
        })
  }

}
