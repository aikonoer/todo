package controllers

import java.io.{File, FileInputStream}
import java.nio.file.Paths
import javax.inject.Inject

import com.google.common.io.ByteStreams
import fly.play.s3.BucketFile
import play.api.Logger
import play.api.mvc._
import services.io.FileIOService
import services.s3.S3Service

import scala.concurrent.ExecutionContext
import scala.util.Try

class IOController @Inject()(cc: ControllerComponents,
                             fileIOService: FileIOService,
                             s3Service: S3Service)
  extends AbstractController(cc)
    with MyBaseController {

  implicit lazy val ec: ExecutionContext = defaultExecutionContext

  def upload = Action { request =>
    val formData = request.body.asMultipartFormData
    formData.flatMap(_.file("picture").map { picture =>

      val filename = s"${formData.get.dataParts("name").head}.${picture.contentType.get.substring(6)} "
      val originalPath = s"/home/marcus/Materials/Projects/Play/todo-app/temp/pictures/$filename"
      val scaledPath = s"/home/marcus/Materials/Projects/Play/todo-app/temp/pictures/scaled/$filename"

      Try {
        picture.ref.moveTo(Paths.get(originalPath), replace = true)
        val writer = fileIOService.getWriter(picture.contentType).get
        fileIOService.scale(Paths.get(originalPath), Paths.get(scaledPath), writer)
        val bucketFile = BucketFile(filename, picture.contentType.get, ByteStreams.toByteArray(new FileInputStream(new File(scaledPath))))
        s3Service.insert(bucketFile)
      }.recover {
        case e: Exception => Logger.info(s"Error occurred: ${e.getMessage}")
      }

      Redirect(routes.IOController.form()).flashing(
        "success" -> "File uploaded")
    }).getOrElse {
      Redirect(routes.IOController.form()).flashing(
        "error" -> "Missing file")
    }


    Logger.info(formData.get.asFormUrlEncoded.get("name").toString)
    Ok("test")
  }

  def form(): Action[AnyContent] = Action { implicit request =>
    Ok(views.html.fileform())
  }

}
