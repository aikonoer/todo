package repositories

import reactivemongo.api.MongoConnection
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}

trait MongoDb extends DbSetup {

  def mongoConnection: MongoConnection
  def connect(db: String = "marcusdb", coll: String)(implicit ec: ExecutionContext): Future[JSONCollection] =
    mongoConnection.database(db).map(_.collection(coll))

}
