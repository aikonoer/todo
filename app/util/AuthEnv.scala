package util

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.silhouettte.UserDetails

trait AuthEnv extends Env {
  override type I = UserDetails
  override type A = CookieAuthenticator
}
