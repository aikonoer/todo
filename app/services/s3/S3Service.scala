package services.s3

import javax.inject.Inject

import fly.play.s3.BucketFile
import repositories.s3.S3Repo

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait S3Service {

  def insert(bucketFile: BucketFile)(implicit ec: ExecutionContext): Future[Either[String, String]]
  def getFile(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, BucketFile]]
  def getUrl(fileName: String): Try[Either[String, String]]
  def delete(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, String]]
}

class S3ServiceImpl @Inject()(s3Repo: S3Repo) extends S3Service {

  def insert(bucketFile: BucketFile)(implicit ec: ExecutionContext): Future[Either[String, String]] = s3Repo.insert(bucketFile)
  def getFile(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, BucketFile]] = s3Repo.getFile(fileName)
  def getUrl(fileName: String): Try[Either[String, String]] = s3Repo.getUrl(fileName)
  def delete(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, String]] = s3Repo.delete(fileName)
}
