package models.silhouettte

import com.mohiva.play.silhouette.api.Identity
import models.Model
import play.api.libs.json.{Json, OFormat}

case class UserDetails(username: String) extends Identity with Model

object UserDetails {
  implicit val userFormat: OFormat[UserDetails] = Json.format[UserDetails]
}

case class PasswordDetails(hasherId: String,
                           username: String,
                           hashedPassword: String,
                           salt: Option[String]) extends Model

object PasswordDetails {
  implicit val passwordFormat: OFormat[PasswordDetails] = Json.format[PasswordDetails]
}

