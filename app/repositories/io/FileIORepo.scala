package repositories.io

import java.nio.file.Path

import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.ScaleMethod.Bicubic
import com.sksamuel.scrimage.nio.ImageWriter

trait FileIORepo {
  def scale(in: Path, out: Path, writer: ImageWriter): Path
}

class FileIORepoImpl extends FileIORepo {

  def scale(in: Path, out: Path, writer: ImageWriter):  Path =
    Image.fromPath(in)
      .scale(0.5, Bicubic)
      .output(out)(writer)

}
