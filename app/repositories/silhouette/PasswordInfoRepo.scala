package repositories.silhouette

import javax.inject.{Inject, Singleton}

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import models.silhouettte.PasswordDetails
import play.api.libs.json.Json
import reactivemongo.api.{MongoConnection, ReadPreference}
import reactivemongo.play.json._
import repositories.MongoDb
import services.silhouette.UserInfoService

import scala.concurrent.{ExecutionContext, Future}

trait PasswordInfoRepo extends DelegableAuthInfoDAO[PasswordInfo] with MongoDb

@Singleton
class PasswordInfoRepoImpl @Inject()(val mongoConnection: MongoConnection,
                                     userInfoService: UserInfoService)(implicit ec: ExecutionContext)
  extends PasswordInfoRepo {

  private def passwordInfoColl = connect(coll = "passwordInfo")

  override def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = {
    for {
      _ <- userInfoService.retrieve(loginInfo)
      c <- passwordInfoColl
      pw <- c.find(Json.obj("username" -> loginInfo.providerKey))
        .one[PasswordDetails](ReadPreference.Primary)
    } yield
      if (pw.isDefined)
        Some(PasswordInfo(pw.get.hasherId, pw.get.hashedPassword, pw.get.salt))
      else None
  }

  override def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    for {
      _ <- userInfoService.retrieve(loginInfo)
      c <- passwordInfoColl
      w <- c.insert(PasswordDetails(authInfo.hasher, loginInfo.providerKey, authInfo.password, authInfo.salt))
      if w.ok
    } yield authInfo
  }


  override def update(loginInfo: LoginInfo, authInfo: PasswordInfo) = ???
  override def save(loginInfo: LoginInfo, authInfo: PasswordInfo) = ???
  override def remove(loginInfo: LoginInfo) = ???


}