package services

import javax.inject.Inject

import models.Note
import repositories.NoteRepo

import scala.concurrent.{ExecutionContext, Future}

trait NoteService extends GenericService[Note] {
  def find(implicit ec: ExecutionContext): Future[Traversable[Note]]
  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]]
}

class NoteServiceImpl @Inject()(noteRepo: NoteRepo) extends NoteService {
  override def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]] = noteRepo.insert(t)

  override def find(implicit ec: ExecutionContext): Future[Traversable[Note]] = noteRepo.find
  override def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]] = noteRepo.findByUsername(username)
}
