package repositories

import models.Model

import scala.concurrent.{ExecutionContext, Future}

trait GenericRepo[T <: Model] {

  def insert(t: T)(implicit ec: ExecutionContext): Future[Either[String, T]]
  def find(implicit ec: ExecutionContext): Future[Traversable[T]]
  def update[T]
  def delete[T]
}

trait DbSetup