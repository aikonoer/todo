package services

import models.Model

import scala.concurrent.{ExecutionContext, Future}

trait GenericService[T <: Model] {

  def insert(t: T)(implicit ec: ExecutionContext): Future[Either[String, T]]
}
