package repositories.silhouette

import javax.inject.{Inject, Singleton}

import com.mohiva.play.silhouette.api.LoginInfo
import models.silhouettte.UserDetails
import play.api.libs.json.Json
import reactivemongo.api.MongoConnection
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import repositories.{GenericRepo, MongoDb}

import scala.concurrent.{ExecutionContext, Future}

trait UserInfoRepo extends MongoDb with GenericRepo[UserDetails] {
  def findByUsername(loginInfo: LoginInfo): Future[Option[UserDetails]]
}

@Singleton
class UserInfoRepoImpl @Inject()(val mongoConnection: MongoConnection)(implicit ec: ExecutionContext) extends UserInfoRepo {

  {
    userInfoColl
      .flatMap(_.indexesManager.create(Index(Seq((
        "username",
        IndexType.Ascending)),
        Some("unique-username"), unique = true)))
  }

  private def userInfoColl: Future[JSONCollection] = connect(coll = "userInfo")

  override def findByUsername(loginInfo: LoginInfo): Future[Option[UserDetails]] =
    userInfoColl
      .flatMap(_.find(Json.obj("username" -> loginInfo.providerKey), Json.obj("_id" -> 0))
        .one[UserDetails])

  override def insert(t: UserDetails)(implicit ec: ExecutionContext): Future[Either[String, UserDetails]] = {
    for {
      userColl <- userInfoColl
      w <- userColl.insert(t)
    } yield
      if (w.ok) Right(t) else Left("Insert not successful")
  }

  override def find(implicit ec: ExecutionContext): Future[Traversable[UserDetails]] = ???

  override def update[T]: Unit = ???

  override def delete[T]: Unit = ???
}


