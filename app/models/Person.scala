package models

import play.api.libs.json.{Json, OFormat}

case class Person(name: String, age: Int) extends Model

object Person {
  implicit val personReads: OFormat[Person] = Json.format[Person]
}
