package repositories.s3

import javax.inject.Inject

import fly.play.s3.{BucketFile, S3, S3Exception}
import play.api.Configuration
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait S3Repo {


  def insert(bucketFile: BucketFile)(implicit ec: ExecutionContext): Future[Either[String, String]]
  def getFile(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, BucketFile]]
  def getUrl(fileName: String): Try[Either[String, String]]
  def delete(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, String]]

}


class S3RepoImpl @Inject()(ws: WSClient,
                           config: Configuration) extends S3Repo {

  private val bucketName = "marcusbuckets"
  private lazy val s3Instance = S3.fromConfiguration(ws, config)
  private lazy val bucket = s3Instance.getBucket(bucketName)

  def insert(bucketFile: BucketFile)(implicit ec: ExecutionContext): Future[Either[String, String]] = {
    bucket.add(bucketFile)
      .map(_ => Right("Success"))
      .recover {
      case S3Exception(status, code, message, _) => Left(s"$status $code $message")
    }
  }

  def getFile(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, BucketFile]] = {
    bucket.get(fileName)
      .map(Right(_))
      .recover{
        case S3Exception(status, code, message, _) => Left(s"$status $code $message")
      }
  }
  def getUrl(fileName: String): Try[Either[String, String]] = {
    Try {
      bucket.url(fileName, 2)
    }
      .map {
        Right(_)
      }.recover {
      case S3Exception(status, code, message, _) => Left(s"$status $code $message")
    }
  }


  def delete(fileName: String)(implicit ec: ExecutionContext): Future[Either[String, String]] = {
    bucket.remove(fileName)
      .map(_ => Right("Success"))
      .recover{
        case S3Exception(status, code, message, _) => Left(s"$status $code $message")
      }
  }

}