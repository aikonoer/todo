package controllers

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "HomeController GET" should {

    /*"render the index page from a new instance of controller" in {
      val controller = new HomeController(stubControllerComponents())
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the application" in {
      val controller = inject[HomeController]
      val home = controller.index().apply(FakeRequest(GET, "/"))

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }

    "render the index page from the router" in {
      val request = FakeRequest(GET, "/")
      val home = route(app, request).get

      status(home) mustBe OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Welcome to Play")
    }*/
    "test akka streams" in {
      import scala.concurrent.ExecutionContext.Implicits._

      implicit val system: ActorSystem = ActorSystem("QuickStart")
      implicit val materializer: ActorMaterializer = ActorMaterializer()

      val source = Source(1 to 100)
      val sink = Sink.fold[Int, Int](0)(_ + _)
      val value1: Future[Int] = source.runWith(sink)
      Await.result(value1, FiniteDuration(2, TimeUnit.SECONDS))
      value1.onComplete {
        case Success(i) => println(i)
        case Failure(_) => println(-1)
      }
    }
  }
}
