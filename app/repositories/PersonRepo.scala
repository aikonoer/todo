package repositories

import javax.inject.{Inject, Singleton}

import models.Person
import play.api.libs.json.{JsObject, Json}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{Cursor, MongoConnection, ReadPreference}
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}


trait PersonRepo extends MongoDb with GenericRepo[Person] {

  def findByName(name: String)(implicit ec: ExecutionContext): Future[Traversable[Person]]
  def findByNameWithProjection(name: String, projection: String)(implicit ec: ExecutionContext): Future[Traversable[JsObject]]
  def deleteByName(name: String)(implicit ec: ExecutionContext): Future[Either[String, String]]
}

@Singleton
class PersonRepoImpl @Inject()(val mongoConnection: MongoConnection) extends PersonRepo {

  private val index = Index(Seq(("name", IndexType.Ascending)), name = Some("name"), unique = true)

  private def connectToPersons(implicit ec: ExecutionContext) = connect(coll = "persons")

  private def indexTitle(implicit ec: ExecutionContext) = connectToPersons
    .flatMap(_.indexesManager.ensure(index))

  private def personsColl(implicit ec: ExecutionContext): Future[JSONCollection] = {
    indexTitle.flatMap(_ => connectToPersons)
  }

  override def insert(person: Person)(implicit ec: ExecutionContext): Future[Either[String, Person]] = {
    personsColl
      .flatMap(_.insert(person)
        .map(_ => Right(person))
        .recover {
          case WriteResult.Message(msg) => Left(msg)
        })
  }

  override def find(implicit ec: ExecutionContext): Future[Traversable[Person]] = genericFind(Json.obj())
  override def findByName(name: String)(implicit ec: ExecutionContext): Future[List[Person]] = genericFind(Json.obj("name" -> name))

  override def deleteByName(name: String)(implicit ec: ExecutionContext): Future[Either[String, String]] = {
    personsColl
      .flatMap(_.remove(Json.obj("name" -> name))
        .map(_ => Right(name))
        .recover {
          case WriteResult.Message(msg) => Left(msg)
        })
  }

  override def update[T]: Unit = ???
  override def delete[T]: Unit = ???

  override def findByNameWithProjection(name: String, projection: String)(implicit ec: ExecutionContext): Future[Traversable[JsObject]] =
    personsColl
    .map(_.find(Json.obj("name" -> name), Json.obj(projection -> 1, "_id" -> 0))
      .cursor[JsObject]())
    .flatMap(_.collect[List](-1, Cursor.FailOnError[List[JsObject]]()))

  private def genericFind(find: JsObject, limit: Int = -1)(implicit ec: ExecutionContext): Future[List[Person]] =
    personsColl
      .map(_.find(find)
        .cursor[Person](ReadPreference.Primary))
      .flatMap(_.collect[List](limit, Cursor.FailOnError[List[Person]]()))
}

