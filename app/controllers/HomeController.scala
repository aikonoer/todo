package controllers

import javax.inject._

import models._
import play.api.Configuration
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.libs.ws.WSClient
import play.api.mvc._
import reactivemongo.play.json._
import services.PersonService

import scala.concurrent.{ExecutionContext, Future}

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               personService: PersonService,
                               config: Configuration,
                               ws: WSClient)
  extends AbstractController(cc) with MyBaseController {

  implicit lazy val ec: ExecutionContext = defaultExecutionContext

  def index() = Action { implicit request =>
//    Logger.info(request.contentType.getOrElse("No content-type"))
    Ok(views.html.index())
  }

  def getJson = Action { implicit request =>
    val person = Person(name = "marcus", age = 1)
    val value: JsValue = Json.toJson(person)
    Ok(value)
  }

  def insertPerson(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[Person].fold(
      error => Future.successful(BadRequest(Json.obj("Status" -> "KO", "message" -> JsError.toJson(error)))),
      person => {
        personService.insert(person)
          .map {
            case Right(p) => Ok(okResponse(Messages("add.success", p.name)))
            case Left(s) => BadRequest(failResponse(Messages("add.failure", person.name, s)))
          }
      })
  }

  def findByName(name: String): Action[AnyContent] = Action.async { implicit request =>
    personService.find(name)
      .map(persons => Ok(Json.toJson(persons)))
      .recover {
        case e: Exception => BadRequest(failResponse(Messages("find.failure", e.getMessage)))
    }
  }

  def findByNameWithProjection(name: String, projection: String): Action[AnyContent] = Action.async { implicit request =>
    personService.find(name, projection).map(names => Ok(Json.toJson(names)))
  }

  def find: Action[AnyContent] = Action.async { implicit request =>
    personService.find.map(all => Ok(Json.toJson(all)))
      .recover{
        case e: Exception => BadRequest(failResponse(Messages("find.failure", e.getMessage)))
      }
  }

  def deleteByName(name: String): Action[AnyContent] = Action.async { implicit request =>
    personService.delete(name)
      .map {
        case Right(n) => Ok(okResponse(Messages("delete.success", n)))
        case Left(e) => BadRequest(failResponse(Messages("delete.failure", e)))
      }
  }
}




