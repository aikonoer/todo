package controllers

import javax.inject.{Inject, Singleton}

import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import services.s3.S3Service

@Singleton
class S3Controller @Inject()(cc: ControllerComponents,
                             s3Service: S3Service)
  extends AbstractController(cc) with MyBaseController {

  def getUrl(fileName: String): Action[AnyContent] = Action { implicit request =>
    s3Service.getUrl(fileName).fold(
      error => BadRequest(failResponse(error.getMessage)), {
        case Right(url) => Ok(okResponse(url))
        case Left(error) => BadRequest(failResponse(error))
      }
    )
  }

}
