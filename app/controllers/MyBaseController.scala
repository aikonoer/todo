package controllers

import play.api.i18n.I18nSupport
import play.api.libs.json.Json

trait MyBaseController extends I18nSupport {

  private[controllers] def okResponse(messages: String) = Json.obj("status" -> "Ok", "message" ->  messages)
  private[controllers] def failResponse(messages: String) = Json.obj("status" -> "Fail", "message" ->  messages)
}
