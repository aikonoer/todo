import com.google.inject.{AbstractModule, Provides, TypeLiteral}
import com.mohiva.play.silhouette.api.crypto._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AuthenticatorService
import com.mohiva.play.silhouette.api.util._
import com.mohiva.play.silhouette.api.{Environment, EventBus, Silhouette, SilhouetteProvider}
import com.mohiva.play.silhouette.crypto._
import com.mohiva.play.silhouette.impl.authenticators.{CookieAuthenticator, CookieAuthenticatorService, CookieAuthenticatorSettings}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import com.mohiva.play.silhouette.impl.util.{DefaultFingerprintGenerator, SecureRandomIDGenerator}
import com.mohiva.play.silhouette.password.BCryptSha256PasswordHasher
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import com.mohiva.play.silhouette.persistence.repositories.DelegableAuthInfoRepository
import play.api.Configuration
import play.api.mvc.CookieHeaderEncoding
import reactivemongo.api.{MongoConnection, MongoDriver}
import repositories.io.{FileIORepo, FileIORepoImpl}
import repositories.s3.{S3Repo, S3RepoImpl}
import repositories.silhouette.{PasswordInfoRepo, PasswordInfoRepoImpl, UserInfoRepo, UserInfoRepoImpl}
import repositories.{NoteRepo, NoteRepoImpl, PersonRepo, PersonRepoImpl}
import services.io.{FileIOService, FileIOServiceImpl}
import services.s3.{S3Service, S3ServiceImpl}
import services.silhouette.{PasswordInfoService, PasswordInfoServiceImpl, UserInfoService, UserInfoServiceImpl}
import services.{PersonService, PersonServiceImpl}
import util.AuthEnv


class Module extends AbstractModule {

  import scala.concurrent.ExecutionContext.Implicits.global

  override def configure(): Unit = {
    bind(classOf[PersonRepo]).to(classOf[PersonRepoImpl]).asEagerSingleton()
    bind(classOf[PersonService]).to(classOf[PersonServiceImpl])
    bind(classOf[NoteRepo]).to(classOf[NoteRepoImpl])
    bind(classOf[MongoConnection]).toInstance {
      val driver = new MongoDriver
      driver.connection(List("localhost"))
    }

    //  Silhouette Bindings
    bind(new TypeLiteral[Silhouette[AuthEnv]]{}).to(new TypeLiteral[SilhouetteProvider[AuthEnv]]{})
    bind(new TypeLiteral[DelegableAuthInfoDAO[PasswordInfo]]{}).to(classOf[PasswordInfoRepoImpl])
    bind(classOf[PasswordInfoRepo]).to(classOf[PasswordInfoRepoImpl])
    bind(classOf[PasswordInfoService]).to(classOf[PasswordInfoServiceImpl])
    bind(classOf[UserInfoRepo]).to(classOf[UserInfoRepoImpl])
    bind(classOf[UserInfoService]).to(classOf[UserInfoServiceImpl])
    bind(classOf[EventBus]).toInstance(EventBus())

    bind(classOf[AuthenticatorEncoder]).to(classOf[Base64AuthenticatorEncoder])
    bind(classOf[IDGenerator]).toInstance(new SecureRandomIDGenerator())
    bind(classOf[Clock]).toInstance(Clock())
    bind(classOf[FingerprintGenerator]).toInstance(new DefaultFingerprintGenerator(false))
    bind(classOf[PasswordHasher]).toInstance(new BCryptSha256PasswordHasher())

    //  S3
    bind(classOf[S3Repo]).to(classOf[S3RepoImpl])
    bind(classOf[S3Service]).to(classOf[S3ServiceImpl])

    //  IO
    bind(classOf[FileIORepo]).to(classOf[FileIORepoImpl])
    bind(classOf[FileIOService]).to(classOf[FileIOServiceImpl])
  }

  @Provides
  def provideEnvironment(userInfoService: UserInfoService,
                         authenticatorService: AuthenticatorService[CookieAuthenticator],
                         eventBus: EventBus): Environment[AuthEnv] =
    Environment[AuthEnv](userInfoService, authenticatorService, Seq(), eventBus)

  @Provides
  def provideAuthenticatorCookieSigner: Signer = {
    new JcaSigner(JcaSignerSettings("changeMe"))
  }

  @Provides
  def provideAuthenticatorCrypter: Crypter = {
    new JcaCrypter(JcaCrypterSettings("changeMe"))
  }

  @Provides
  def provideAuthenticatorService(
                                   jcaSigner: Signer,
                                   authenticatorEncoder: AuthenticatorEncoder,
                                   defaultCookieHeaderEncoding: CookieHeaderEncoding,
                                   fingerprintGenerator: FingerprintGenerator,
                                   iDGenerator: IDGenerator,
                                   configuration: Configuration,
                                   clock: Clock): AuthenticatorService[CookieAuthenticator] = {

    new CookieAuthenticatorService(CookieAuthenticatorSettings(secureCookie = false),
      None,
      jcaSigner,
      defaultCookieHeaderEncoding,
      authenticatorEncoder,
      fingerprintGenerator,
      iDGenerator,
      clock)
  }

  @Provides
  def provideAuthInfoRepository(passwordInfoRepo: DelegableAuthInfoDAO[PasswordInfo]): AuthInfoRepository = {
    new DelegableAuthInfoRepository(passwordInfoRepo)
  }

  @Provides
  def CredentialsProvider(
                           authInfoRepository: AuthInfoRepository,
                           passwordHasher: PasswordHasher): CredentialsProvider = {
    new CredentialsProvider(authInfoRepository, PasswordHasherRegistry(passwordHasher))
  }
}
