package services

import javax.inject.Inject

import models.Person
import play.api.libs.json.JsObject
import repositories.PersonRepo

import scala.concurrent.{ExecutionContext, Future}

trait PersonService extends GenericService[Person]{

  def find(implicit ec: ExecutionContext): Future[Traversable[Person]]
  def find(name: String)(implicit ec: ExecutionContext): Future[Traversable[Person]]
  def find(name: String, projection: String)(implicit ec: ExecutionContext): Future[Traversable[JsObject]]
  def delete(name: String)(implicit ec: ExecutionContext): Future[Either[String, String]]
}

class PersonServiceImpl @Inject()(personRepo: PersonRepo) extends PersonService {

  def insert(t: Person)(implicit ec: ExecutionContext): Future[Either[String, Person]] = personRepo.insert(t)
  def find(implicit ec: ExecutionContext): Future[Traversable[Person]] = personRepo.find
  def find(name: String)(implicit ec: ExecutionContext): Future[Traversable[Person]] = personRepo.findByName(name)
  def find(name: String, projection: String)(implicit ec: ExecutionContext): Future[Traversable[JsObject]] = personRepo.findByNameWithProjection(name, projection)
  def delete(name: String)(implicit ec: ExecutionContext): Future[Either[String, String]] = personRepo.deleteByName(name)

}
